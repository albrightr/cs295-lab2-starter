<!DOCTYPE html>
<!--
Author: Rachel Albright
Date: 01/21/13
Purpose: Lab2 PHP forms
-->
<?php
$ingredient0= htmlentities($_POST['ingredient0']);
$ingredient1= htmlentities($_POST['ingredient1']);
$ingredient2= htmlentities($_POST['ingredient2']);

?>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
    </head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1> 
			<h2>Thank you for submitting a recipe for:<em> <?php echo htmlentities($_POST['title']);?> </em>
			</h2>
	        
			<h3>Ingredients</h3>
			<ul>
				<li> <?php echo $ingredient0;?></li>
				<li> <?php echo $ingredient1;?></li>
				<li> <?php echo $ingredient2?></li>
			</ul>
			<h3>Instructions</h3>
			<p><?php echo htmlentities($_POST['instructions']);?>
			
			</p>
		</div>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
